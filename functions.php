<?php

if(isset($_GET['debug'])){
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
}

$currentTheme = wp_get_theme();

$currentLocale = get_locale();
if(!is_admin()){
    setlocale(LC_ALL, $currentLocale . '.utf8');
}

define('THEME_TEXTDOMAIN', $currentTheme->get('TextDomain'));
define('THEME_OPTIONS_NAME', 'pyramid');
define('THEME_DIR', get_template_directory());
define('THEME_URI', get_template_directory_uri());
define('WOOCOMMERCE_USE_CSS', false);

load_theme_textdomain( THEME_TEXTDOMAIN, get_template_directory().'/languages' );

$themeOptions = array(
    'textdomain' => THEME_TEXTDOMAIN,
    'twig' => array(
        'debug' => false,
        'paths' => array(
            'svg' => get_template_directory() . '/assets/svg'
        )
    ),
    'adminAssets' => array(
        'css' => get_template_directory_uri() . '/assets/css/admin.css',
        'js' => false,
        'editorStyle' => false
    ),
    'disableJquery' => false, //load from cdn in includes/theme.functions.php
    'addScriptJs' => true,
	'useWoocommerce' => true
);

add_filter('tr_theme_options_name', function() {
    return THEME_OPTIONS_NAME;
});
add_filter('tr_theme_options_page', function() {
    return THEME_DIR . '/theme-options.php';
});

require_once('typerocket/init.php');

require_once('core/core.php');

$tweaks = glob(get_template_directory()."/tweaks/*.php");
if(is_array($tweaks)){
    foreach($tweaks as $tweak){
        require_once($tweak);
    }
}

$includes = glob(get_template_directory()."/includes/*.php");
if(is_array($includes)){
    foreach($includes as $include){
        require_once($include);
    }
}