<?php
/* Template Name: Kapcsolat oldal */

the_post();

$data = array(
    'title' => get_the_title(),
    'content' => apply_filters('the_content', get_the_content())
);

twig_render('pages/template-contact.twig', $data);