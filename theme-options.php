<?php
if ( ! function_exists( 'add_action' )) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}
// Setup Form
$form = tr_form()->useJson()->setGroup( $this->getName() );
?>

<h1>Sablon beállításai</h1>
<div class="typerocket-container">
    <?php
    echo $form->open();

    // API
    $api = function() use ($form) {
        echo $form->text( 'analytics')
            ->setLabel('Google Analytics azonosító');
    };

    // Save
    $save = $form->submit( 'Mentés' );

    // Layout
    tr_tabs()->setSidebar( $save )
        ->addTab( 'Google', $api )
        ->render( 'box' );

    echo $form->close();
    ?>

</div>