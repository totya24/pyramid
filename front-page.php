<?php

the_post();

$data = [
    'title' => get_the_title(),
    'content' => apply_filters('the_content', get_the_content()),
    'categories' => [],
    'subcategories' => [],
    'products' => [],
    'slider' => Slider::getSlider()
];

$productCategories = get_terms( 'product_cat', ['orderby' => 'name','order' => 'asc', 'hide_empty' => true] );
if(!empty($productCategories)){
    foreach($productCategories as $c){
        if($c->parent == 0){
            $data['categories'][] = [
                'name' => $c->name,
                'slug' => $c->slug,
                'url' => get_term_link($c),
                'image' => wp_get_attachment_image_src(get_term_meta($c->term_id, 'thumbnail_id', true), 'large')
            ];
        } else {
            $data['subcategories'][] =[
                'name' => $c->name,
                'slug' => $c->slug,
                'url' => get_term_link($c),
                'image' => wp_get_attachment_image_src(get_term_meta($c->term_id, 'thumbnail_id', true), 'large-thumb')
            ];
        }
    }
}

$featuredProducts = wc_get_products([
    'orderby'  => 'name',
    'featured' => true,
    'limit' => 4,
]);

if(!empty($featuredProducts)){
    foreach ($featuredProducts as $product) {
        $data['products'][] = [
            'name' => $product->get_name(),
            'slug' => $product->get_slug(),
            'url' => $product->get_permalink(),
            'image' => wp_get_attachment_image_src( $product->get_image_id(), 'large' ),
            'price' => $product->get_price_html(),
            'currency' => get_woocommerce_currency_symbol()
        ];
    }
}

$blogPosts = wp_get_recent_posts(['posts_per_page' => 4]);
if(!empty($blogPosts)){
    foreach($blogPosts as $blogPost){
        $data['blogposts'][] = [
            'title' => $blogPost['post_title'],
            'excerpt' => get_the_excerpt($blogPost['ID']),
            'image' => get_the_post_thumbnail_url( $blogPost['ID'], array(500) ),
            'url' => get_permalink($blogPost['ID']),
        ];
    }
}

//greedo::var_dump($data);

twig_render('pages/front-page.twig', $data);