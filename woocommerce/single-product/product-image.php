<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.5.1
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
    return;
}

function gcd($a, $b) {
    if ($a == 0 || $b == 0)
        return abs(max(abs($a), abs($b)));
    $r = $a % $b;
    return ($r != 0) ? gcd($b, $r) : abs($b);
}

function ratio($a = null, $b = null)
{
    if(empty($a) || empty($b)) return 1;
    $gcd = gcd($a, $b);
    return ($a / $gcd) . ':' . ($b / $gcd);
}


global $product;

$pThumb = $product->get_image_id();
$pGal = $product->get_gallery_image_ids();

$pImages = array_merge((array)(int)$pThumb,  $pGal);

$mainImage = wp_get_attachment_image_src( $pImages[0], 'large' );

$ratio = ratio((int)$mainImage[1], (int)$mainImage[2]);

?>
<div uk-slideshow="ratio: <?php echo $ratio; ?>">
    <ul class="uk-slideshow-items" uk-lightbox>
        <?php
        if(!empty($pImages)){
            foreach($pImages as $imgId){
                echo '<li><a href="'. wp_get_attachment_image_src( $imgId, 'full' )[0] .'"><img src="'. $mainImage[0] .'" srcset="'.wp_get_attachment_image_srcset( $imgId ).'" alt=""></a></li>';
            }
        }
        ?>
    </ul>

    <div class="uk-card-body uk-flex uk-flex-center">
        <div class="uk-width-1-2 uk-visible@s">
            <div uk-slider="finite: true">
                <div class="uk-position-relative">
                    <div class="uk-slider-container">
                        <ul class="tm-slider-items uk-slider-items uk-child-width-1-4 uk-grid uk-grid-small">
                        
                        <?php
                        if(!empty($pImages)){
                            foreach($pImages as $i => $imgId){
                                ?>
                                <li uk-slideshow-item="<?php echo $i ?>">
                                <img src="<?php echo wp_get_attachment_image_src( $imgId, 'small' )[0]; ?>">
                                </li>
                                <?php
                            }
                        }
                        ?>
                            
                        </ul>
                        <div><a class="uk-position-center-left-out uk-position-small" href="#" uk-slider-item="previous"
                                uk-slidenav-previous></a><a class="uk-position-center-right-out uk-position-small" href="#"
                                uk-slider-item="next" uk-slidenav-next></a></div>
                    </div>
                </div>
            </div>
        </div>
        <ul class="uk-slideshow-nav uk-dotnav uk-hidden@s"></ul>
    </div>
</div>

