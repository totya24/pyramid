<?php
/**
 * Checkout billing information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 * @global WC_Checkout $checkout
 */

defined( 'ABSPATH' ) || exit;
?>
<div class="uk-card uk-card-default uk-card-small uk-card-body">
	<?php if ( wc_ship_to_billing_address_only() && WC()->cart->needs_shipping() ) : ?>

		<h3 class="uk-card-title"><?php esc_html_e( 'Billing &amp; Shipping', 'woocommerce' ); ?></h3>

	<?php else : ?>

		<h3 class="uk-card-title"><?php esc_html_e( 'Billing details', 'woocommerce' ); ?></h3>

	<?php endif; ?>

	<?php do_action( 'woocommerce_before_checkout_billing_form', $checkout ); ?>

	<div uk-grid>
		<?php
		$fields = $checkout->get_checkout_fields( 'billing' );

		$fieldClasses = [
			'billing_first_name' => 'uk-width-1-2@s',
			'billing_last_name' => 'uk-width-1-2@s',
			'billing_country' => 'uk-width-1-3@s',
			'billing_address_1' => 'uk-width-1-2@s',
			'billing_address_2' => 'uk-width-1-2@s',
			'billing_city' => 'uk-width-1-3@s',
			'billing_postcode' => 'uk-width-1-3@s',
			'billing_phone' => 'uk-width-1-2@s',
			'billing_email' => 'uk-width-1-2@s',
		];

		$typeClasses = [
			'country' => 'uk-select',
			'state' => 'uk-select',
			'textarea' => 'uk-textarea',
			'checkbox' => 'uk-checkbox',
			'select' => 'uk-select',
			'radio' => 'uk-radio',
		];

		foreach ( $fields as $key => $field ) {
			$field['label_class'] = 'uk-form-label';
			
			if(!empty($field['type']) && !empty($typeClasses[$field['type']])){
				$field['input_class'][] = $typeClasses[$field['type']];
			} else {
				$field['input_class'][] = 'uk-input';
			}
			
			if(!empty($fieldClasses[$key])){
				$field['class'][] = $fieldClasses[$key];
			}
			
			woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
		}
		?>
	</div>

	<?php do_action( 'woocommerce_after_checkout_billing_form', $checkout ); ?>
</div>

<?php if ( ! is_user_logged_in() && $checkout->is_registration_enabled() ) : ?>
	<div class="uk-margin-top uk-card uk-card-default uk-card-body uk-card-small">
		<?php if ( ! $checkout->is_registration_required() ) : ?>

			<p class="form-row form-row-wide create-account">
				<label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox">
					<input class="uk-checkbox uk-margin-small-right" id="createaccount" <?php checked( ( true === $checkout->get_value( 'createaccount' ) || ( true === apply_filters( 'woocommerce_create_account_default_checked', false ) ) ), true ); ?> type="checkbox" name="createaccount" value="1" /> <span><?php esc_html_e( 'Create an account?', 'woocommerce' ); ?></span>
				</label>
			</p>

		<?php endif; ?>

		<?php do_action( 'woocommerce_before_checkout_registration_form', $checkout ); ?>

		<?php if ( $checkout->get_checkout_fields( 'account' ) ) : ?>

			<div class="create-account">
				<?php foreach ( $checkout->get_checkout_fields( 'account' ) as $key => $field ) : ?>
					<?php 
						$field['input_class'][] = 'uk-input';
						$field['input_class'][] = 'uk-width-1-3@s';
						woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
					?>
				<?php endforeach; ?>
				<div class="clear"></div>
			</div>

		<?php endif; ?>

		<?php do_action( 'woocommerce_after_checkout_registration_form', $checkout ); ?>
	</div>
<?php endif; ?>
