<?php
/**
 * Checkout shipping information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-shipping.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 * @global WC_Checkout $checkout
 */

defined( 'ABSPATH' ) || exit;
?>
<div class="uk-card uk-card-default uk-card-small uk-card-body uk-margin-top">
	<?php if ( true === WC()->cart->needs_shipping_address() ) : ?>

		<h3 id="uk-card-title ship-to-different-address">
			<label class="woocommerce-form__label woocommerce-form__label-for-checkbox checkbox" uk-toggle="target: .shipping_address; animation: uk-animation-fade">
				<input id="ship-to-different-address-checkbox" class="uk-checkbox uk-margin-small-right" <?php checked( apply_filters( 'woocommerce_ship_to_different_address_checked', 'shipping' === get_option( 'woocommerce_ship_to_destination' ) ? 1 : 0 ), 1 ); ?> type="checkbox" name="ship_to_different_address" value="1" /> <span><?php esc_html_e( 'Ship to a different address?', 'woocommerce' ); ?></span>
			</label>
		</h3>

		<div class="shipping_address" hidden>

			<?php do_action( 'woocommerce_before_checkout_shipping_form', $checkout ); ?>

			<div class="woocommerce-shipping-fields__field-wrapper" uk-grid>
				<?php
				$fields = $checkout->get_checkout_fields( 'shipping' );

				$fieldClasses = [
					'shipping_first_name' => 'uk-width-1-2@s',
					'shipping_last_name' => 'uk-width-1-2@s',
					'shipping_country' => 'uk-width-1-3@s',
					'shipping_address_1' => 'uk-width-1-2@s',
					'shipping_address_2' => 'uk-width-1-2@s',
					'shipping_city' => 'uk-width-1-3@s',
					'shipping_postcode' => 'uk-width-1-3@s',
					'shipping_phone' => 'uk-width-1-2@s',
					'shipping_email' => 'uk-width-1-2@s',
				];

				$typeClasses = [
					'country' => 'uk-select',
					'state' => 'uk-select',
					'textarea' => 'uk-textarea',
					'checkbox' => 'uk-checkbox',
					'select' => 'uk-select',
					'radio' => 'uk-radio',
				];

				foreach ( $fields as $key => $field ) {
					$field['label_class'] = 'uk-form-label';
					
					if(!empty($typeClasses[$field['type']])){
						$field['input_class'][] = $typeClasses[$field['type']];
					} else {
						$field['input_class'][] = 'uk-input';
					}

					if(!empty($fieldClasses[$key])){
						$field['class'][] = $fieldClasses[$key];
					}

					woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
				}
				?>
			</div>

			<?php do_action( 'woocommerce_after_checkout_shipping_form', $checkout ); ?>

		</div>

	<?php endif; ?>
</div>

<div class="uk-card uk-card-default uk-card-small uk-card-body uk-margin-top">
	<?php do_action( 'woocommerce_before_order_notes', $checkout ); ?>

	<?php if ( apply_filters( 'woocommerce_enable_order_notes_field', 'yes' === get_option( 'woocommerce_enable_order_comments', 'yes' ) ) ) : ?>

		<?php if ( ! WC()->cart->needs_shipping() || wc_ship_to_billing_address_only() ) : ?>

			<h3 class="uk-card-title"><?php esc_html_e( 'Additional information', 'woocommerce' ); ?></h3>

		<?php endif; ?>

		<div class="woocommerce-additional-fields__field-wrapper" uk-grid>
			<?php foreach ( $checkout->get_checkout_fields( 'order' ) as $key => $field ) {
				$field['input_class'][] = 'uk-textarea';
				$field['label_class'] = 'uk-form-label';
				$field['class'][] = 'uk-width-1-1';
				woocommerce_form_field( $key, $field, $checkout->get_value( $key ) ); 
			} ?>
		</div>

	<?php endif; ?>

	<?php do_action( 'woocommerce_after_order_notes', $checkout ); ?>
</div>