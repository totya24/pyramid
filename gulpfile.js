'use strict';

let gulp       = require('gulp'),
    sass       = require('gulp-sass'),
	cssnano    = require('gulp-cssnano'),
	cleanCSS = require('gulp-clean-css'),
    rename     = require('gulp-rename');

gulp.task('sass:dev', function () {
	return gulp.src('src/scss/site.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(rename('style.min.css'))
		.pipe(gulp.dest('assets/css/'));
});

gulp.task('sass', function () {
	return gulp.src('src/scss/site.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(cleanCSS({compatibility: '*', level: 2}))
		.pipe(cssnano())
		.pipe(rename('style.min.css'))
		.pipe(gulp.dest('assets/css/'));
});
 
gulp.task('sass:watch', function() {
	gulp.watch('src/scss/**/*.scss', gulp.series('sass:dev'));
});