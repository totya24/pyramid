<?php

Class PyramidWoocommerce extends Singleton 
{
    public function __construct()
    {
        add_filter( 'woocommerce_enqueue_styles', '__return_empty_array' );

        //remove breadcrumbs
        remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );

        //remove results and order select - product archive
        remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
        remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
        remove_action( 'woocommerce_after_shop_loop_item' , 'woocommerce_template_loop_add_to_cart', 10);

        add_action( 'woocommerce_before_shop_loop', [$this, 'productFilter'], 30 );
        add_action( 'woocommerce_product_query', [$this, 'applyProductFilter'] );

        add_filter( 'loop_shop_per_page', [$this, 'newLoopShopPerPage'], 20 );
        add_filter( 'woocommerce_pagination_args', [$this, 'paginationArgs'], 10, 1 ); 
        add_filter( 'woocommerce_product_loop_title_classes', [$this, 'productTitleClasses'], 10, 1 ); 
        add_filter( 'woocommerce_product_get_image' , [$this, 'productImage'], 10, 6);

        // single product page
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

        //remove all unnecessary scripts from footer
        add_action( 'template_redirect', [ $this, 'removeWoocommerceStylesScripts'], 999 );
        
        add_filter( 'wc_product_sku_enabled', '__return_false' );
        
        // Checkout
        add_filter( 'woocommerce_checkout_fields', [$this, 'removeCheckoutFields'], 10 );
        add_filter( 'woocommerce_cart_needs_shipping_address', '__return_true', 50 );
        add_filter( 'woocommerce_cart_needs_shipping', '__return_true', 50 ); 
        add_filter( 'woocommerce_form_field', [$this, 'customCheckoutField'], 10, 4 );
        add_filter( 'woocommerce_default_address_fields', [$this, 'changeAddressFields'], 10, 1 );
        add_filter( 'woocommerce_cart_shipping_method_full_label', [$this, 'changeShippingLabel'], 10, 2);

        //cart shipping calculator
        add_filter( 'woocommerce_shipping_calculator_enable_state', '__return_false');

        //account page
        add_filter( 'woocommerce_account_menu_items', [$this, 'removeDownloadTab'], 999 );
        

        $this->productPage();
    }

    function removeDownloadTab( $items ) {
        unset($items['downloads']);
        return $items;
    }

    public function changeShippingLabel($label, $method)
    {
        $label = str_replace('amount"', 'uk-text-bold amount"', $label);
        return $label;
    }

    public function customCheckoutField($field)
    {
        $field = preg_replace(
            '#<span class="woocommerce-input-wrapper">(.*?)</span>#',
            '<div class="uk-form-controls">$1</div>',
            $field
        );

        $field = preg_replace(
            '#<p class="form-row (.*?)"(.*?)>(.*?)</p>#',
            '<div class="form-row $1"$2>$3</div>',
            $field
        );
        return $field;
    }

    public function removeCheckoutFields($fields)
    {
        unset( $fields ['billing'] ['billing_company'] );
        unset( $fields ['shipping'] ['shipping_company'] );
        return $fields;
    }

    public function changeAddressFields($fields)
    {
        unset( $fields['state'] );
        $fields['postcode']['priority'] = 68;
        $fields['last_name']['priority'] = 9;
        $fields['address_1']['priority'] = 92;
        $fields['address_2']['priority'] = 94;
        return $fields;
    }

    public function removeWoocommerceStylesScripts()
    {
        if ( function_exists( 'is_woocommerce' ) ) {
            if ( ! is_woocommerce() && ! is_cart() && ! is_checkout() ) {
                remove_action('wp_enqueue_scripts', [WC_Frontend_Scripts::class, 'load_scripts']);
                remove_action('wp_print_scripts', [WC_Frontend_Scripts::class, 'localize_printed_scripts'], 5);
                remove_action('wp_print_footer_scripts', [WC_Frontend_Scripts::class, 'localize_printed_scripts'], 5);
            }
        }
    }

    public function productPage()
    {
        remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
        add_filter('woocommerce_product_description_heading', '__return_null');

        add_action( 'woocommerce_single_product_summary', function(){
            woocommerce_get_template( 'single-product/tabs/description.php' );
        }, 20 );
        
    }

    public function productFilter()
    {
        global $wpdb;
        $data = [];

        /* DEFAULT WOOCOMMERCE ORDERING METHOD */

        $orderby = isset( $_GET['orderby'] ) ? wc_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) ); 
        $show_default_orderby = 'menu_order' === apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) ); 
        $catalog_orderby_options = apply_filters( 'woocommerce_catalog_orderby', array( 
            'menu_order' => __( 'Alapértelmezett rendezés', THEME_TEXTDOMAIN ),  
            'popularity' => __( 'Népszerűség szerint', THEME_TEXTDOMAIN ),  
            'rating' => __( 'Értékelés szerint', THEME_TEXTDOMAIN ),  
            'date' => __( 'Újdonság szerint', THEME_TEXTDOMAIN ),  
            'price' => __( 'Ár szerint növekvő', THEME_TEXTDOMAIN ),  
            'price-desc' => __( 'Ár szerint csökkenő', THEME_TEXTDOMAIN ),  
        ) ); 
    
        if ( ! $show_default_orderby ) { 
            unset( $catalog_orderby_options['menu_order'] ); 
        } 
    
        if ( 'no' === get_option( 'woocommerce_enable_review_rating' ) ) { 
            unset( $catalog_orderby_options['rating'] ); 
        } 
    
        $data['orderselect'] = [
            'options' => $catalog_orderby_options,
            'active' => $orderby,
            'label' => __('Rendezés', THEME_TEXTDOMAIN),
        ]; 

        /* CUSTOM PRODUCT ATTRIBUTE ORDERING METHOD */

        $term = get_queried_object();
        if(!empty($term)){
            $children = get_term_children($term->term_id, $term->taxonomy);
        }

        if(!empty($children)){
            $childIds = implode(',', $children);
            
            $attributes = $wpdb->get_results('SELECT m.meta_value FROM '. $wpdb->prefix .'postmeta AS m INNER JOIN '. $wpdb->prefix .'term_relationships AS r ON r.object_id = m.post_id WHERE r.term_taxonomy_id IN ('. $childIds .') AND m.meta_key="_product_attributes" AND m.meta_value LIKE "%-meret%"');
        } else {
            $attributes = $wpdb->get_results('SELECT meta_value FROM '. $wpdb->prefix .'postmeta WHERE meta_key ="_product_attributes" AND meta_value LIKE "%-meret%"');
        }

        $topSizes = [];
        $bottomSizes = [];

        $topSizesOrdered = [
            'A' => [],
            'B' => [],
            'C' => [],
            'D' => [],
            'E' => [],
            'F' => [],
        ];

        $topSizesSorted = [];

        if(is_array($attributes)){
            foreach($attributes as $f){
                $value = unserialize($f->meta_value);
                $values = [];
                if(!empty($value['felso-meret'])){
                    $values = explode(" | ", $value['felso-meret']['value']);
                }
                if(is_array($values)){
                    foreach($values as $v){
                        $v = trim($v);
                        if(!empty($v) && array_search($v, $topSizes) === false){
                            $topSizes[] = $v;

                            $int =  substr($v, 0,-1);
                            $chr  = substr($v, -1);

                            if(array_search($int, $topSizesOrdered[$chr]) == false){
                                $topSizesOrdered[$chr][] = $int;
                            }
                        }
                    }
                }
                $values = explode(" | ", $value['also-meret']['value']);
                if(is_array($values)){
                    foreach($values as $v){
                        $v = trim($v);
                        if(array_search($v, $bottomSizes) === false){
                            $bottomSizes[] = $v;
                        }
                    }
                }
            }
        }

        foreach($topSizesOrdered as $key => $value){
            if(!empty($value)){
                sort($value);
                
                foreach($value as $v){
                    $topSizesSorted[] = $v.$key;
                }
                
            }
        }

        $data['selects']['ts'] = [
            'options' => $topSizesSorted,
            'active' => !empty($_GET['ts']) ? $_GET['ts'] : null,
            'all' => __('Minden méret', THEME_TEXTDOMAIN),
            'label' => __('Felső méret', THEME_TEXTDOMAIN),
        ];

        $data['selects']['bs'] = [
            'options' => $bottomSizes,
            'active' => !empty($_GET['bs']) ? $_GET['bs'] : null,
            'all' => __('Minden méret', THEME_TEXTDOMAIN),
            'label' => __('Alsó méret', THEME_TEXTDOMAIN),
        ];

        $data['total'] = [
            'total'    => wc_get_loop_prop( 'total' ),
			'per_page' => wc_get_loop_prop( 'per_page' ),
			'current'  => wc_get_loop_prop( 'current_page' ), 
        ];
        
        twig_render('woocommerce/filters.twig', $data, true);
    }

    public function applyProductFilter( $q )
    {
    
        $metaQuery = array('relation' => 'AND');
        
        //top size
        if(!empty($_GET['ts'])){
            $metaQuery[] = array(
                'key'     => '_product_attributes',
                'value'   => $_GET['ts'],
                'compare' => 'LIKE',
            );
        }
        
        //bottom size
        if(!empty($_GET['bs'])){
            $metaQuery[] = array(
                'key'     => '_product_attributes',
                'value'   => $_GET['bs'],
                'compare' => 'LIKE',
            );
        }
        
        $q->set( 'meta_query', $metaQuery );
    }

    function newLoopShopPerPage( $cols )
    {
        return 16;
    }

    public function paginationArgs( $array )
    { 
        return array( 
            'prev_text' => '<span uk-pagination-previous></span>',
            'next_text' => '<span uk-pagination-next></span>',
            'type' => 'list',
            'end_size' => 3,
            'mid_size' => 3
        );
        
    }

    public function productTitleClasses($classes)
    {
        return $classes.' uk-link-reset product-title';
    }

    public function productImage($thiis, $size, $attr, $placeholder, $image)
    {
        $thiis = str_replace('size-woocommerce_thumbnail', 'size-woocommerce_thumbnail uk-transition-scale-up uk-transition-opaque', $thiis);
        return $thiis;
    }

}

PyramidWoocommerce::getinstance();