<?php

Class AjaxCart extends Singleton 
{
    public function __construct()
    {
        add_action('wp_ajax_ajaxAddToCart', [$this, 'addToCart']);
        add_action('wp_ajax_nopriv_ajaxAddToCart', [$this, 'addToCart']);
        //remove_action( 'wp_loaded', array( 'WC_Form_Handler', 'add_to_cart_action' ), 20 );

        add_filter( 'woocommerce_add_to_cart_fragments', [$this, 'addCartFragment'] );
    }

    function addCartFragment( $fragments )
    {
        global $woocommerce;
        $count = $woocommerce->cart->cart_contents_count;
        $fragments['#cart_counter'] = '<span id="cart_counter" class="uk-badge'. ($count == 0 ? ' uk-background-muted uk-text-muted' : '') .'">' . $count . '</span>';
        return $fragments;
    }

    public function addToCart()
    {
        //$product_id = apply_filters( 'woocommerce_add_to_cart_product_id', absint( $_POST['product_id'] ) );
        //$quantity = empty( $_POST['quantity'] ) ? 1 : apply_filters( 'woocommerce_stock_amount', $_POST['quantity'] );
        //$variation_id = $_POST['variation_id'];
        //$variation  = $_POST['variation'];

        $errors = [];

        if(!empty($_POST['required_attributes'])){
            foreach($_POST['required_attributes'] as $a){
                if(empty($_POST[$a])){
                    $errors[$a] = __('Kérlek, válassz egy opciót!', THEME_TEXTDOMAIN);
                }
            }
        }

        if(!empty($errors)){
            wc_clear_notices();
            wp_send_json_error( ['errors' => $errors] );
        }

        $passed_validation = apply_filters( 'woocommerce_add_to_cart_validation', true, $product_id, $quantity, $variation_id, $variation );

        if ( $passed_validation  ) {

            wc_clear_notices();
            WC_AJAX::get_refreshed_fragments();
        }  else  {
            $data = array(
                'product_url' => apply_filters( 'woocommerce_cart_redirect_after_error', get_permalink( $product_id ), $product_id  )
            );
            wp_send_json_error( $data );
        }
        die();
    }
}

AjaxCart::getInstance();