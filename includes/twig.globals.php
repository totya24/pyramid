<?php

Class TwigGlobals extends Singleton 
{
    public function __construct()
    {
        if(!is_admin()){
            add_filter('twig_post_template_vars', array($this, 'addTwigGlobals'));
        }
    }
    
    public function addTwigGlobals( $globals )
    {
        $isAjax = wp_doing_ajax();

        $themeDirectory = get_template_directory();
        
        $themeData = wp_get_theme();
        
        $privacyPolicyPageId = get_option( 'wp_page_for_privacy_policy' );
        
        $theHeader = '';
        $theFooter = '';
        $languageAttributes = '';
        
        if(!$isAjax){
            /*ob_start();
            wp_head();
            $theHeader = ob_get_clean();
            */

            /*$hook_name = 'wp_print_footer_scripts';
            global $wp_filter;*/
            //var_dump($wp_filter[$hook_name]);
            //remove_action('wp_print_footer_scripts', '_wp_footer_scripts', 10);
            //die();

            /*ob_start();
            wp_footer();
            $theFooter = ob_get_clean();

            $theFooter = str_replace("script type='text/javascript' src", "script type='text/javascript' defer src", $theFooter);*/
            
            ob_start();
            language_attributes();
            $languageAttributes = ob_get_clean();
        }
        
        $themeSettings = get_option('pyramid');
        
        $globals['site'] = array(
            'title' => ThemeFunctions::getTitle() . ' - ' . get_bloginfo( 'name' ),
            'charset' => get_bloginfo( 'charset' ),
            'bodyClass' => implode( ' ', get_body_class() ),
            'languageAttributes' => $languageAttributes,
            'lang' => get_locale(),
            'baseUrl' => get_bloginfo( 'url' ),
            'themeUrl' => get_stylesheet_directory_uri(),
            'ajaxUrl' => admin_url( 'admin-ajax.php' ),
            'cssFileTime' => 0,
            'themeSettings' => $themeSettings,
            //'headerContent' => $theHeader,
            //'footerContent' => $theFooter,
            'privacyPolicyPage' => get_post_status( $privacyPolicyPageId ) == 'publish',
            'privacyPolicyUrl' => get_permalink( $privacyPolicyPageId ),
            'woocommerce' => array(
                'accountUrl' => get_permalink( wc_get_page_id( 'myaccount' ) ),
                'checkoutUrl' => get_permalink( wc_get_page_id( 'checkout' ) ),
                'cartUrl' => wc_get_cart_url(),
                'cartCount' => WC()->cart->get_cart_contents_count(),
                'cartTotal' => WC()->cart->get_cart_total()
            )
        );
        
        if( file_exists($themeDirectory . '/assets/css/style.min.css') ){
            $globals['site']['cssFileTime'] = filemtime( $themeDirectory . '/assets/css/style.min.css' );
        }
        return $globals;
    }
}

TwigGlobals::getInstance();