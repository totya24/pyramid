<?php

Class Slider extends Singleton 
{
    public function __construct()
    {
        $slider = tr_post_type('Slider', 'Slider');
        $slider->setAdminOnly();
        $slider->setIcon('images');
        $slider->setArgument('supports', ['title', 'thumbnail', 'page-attributes'] );

        $slider->setEditorForm(function() {
            $form = tr_form();
            echo $form->toggle('slidetype')->setLabel('Cím, alcím és CTA megjelenítése')->setText('Igen');
            echo $form->text('subtitle')->setLabel('Alcím');
            echo $form->text('ctatitle')->setLabel('CTA gomb szövege');
            echo $form->text('ctaurl')->setLabel('CTA gomb link');
        });

        add_filter( 'post_order_types', function($types){ $types[] = 'slider'; return $types; } );
    }

    public static function getSlider( $limit = -1 )
    {
        $sliders = array();
            
        $slidersRaw = get_posts(array(
            'post_type' => 'slider',
            'posts_per_page' => $limit,
            'orderby' => 'menu_order',
            'order' => 'ASC'
        ));
        
        if(is_array($slidersRaw)){
            foreach($slidersRaw as $s){
                
                $slider = array(
                    'id' => $s->ID,
                    'title' => $s->post_title,
                    'image' => get_the_post_thumbnail_url($s->ID, 'large'),
                    'showcta' => tr_posts_field('slidetype', $s->ID),
                    'subtitle' => tr_posts_field('subtitle', $s->ID),
                    'cta' => tr_posts_field('ctatitle', $s->ID),
                    'url' => tr_posts_field('ctaurl', $s->ID)
                );
                
                $sliders[] = $slider;
            }
        }
        return $sliders;
    }
}

Slider::getInstance();