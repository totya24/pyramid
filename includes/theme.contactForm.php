<?php

Class ContactForm extends Singleton 
{
    public function __construct()
    {
        add_action( 'wp_ajax_contactFormSubmit', array($this, '_sendContactForm') );
        add_action( 'wp_ajax_nopriv_contactFormSubmit', array($this, '_sendContactForm') );
    }

    public function _sendContactForm()
    {
        $errors = array();

        if(empty($_POST['name'])){
            $errors['name'] = 'Kérlek, add meg a neved!';
        }

        if(empty($_POST['message'])){
            $errors['message'] = 'Az üzenet szövege nem lehet üres!';
        }

        if(empty($_POST['email'])){
            $errors['email'] = __('Kérlek, add meg az email címed!', THEME_TEXTDOMAIN);
        } elseif (!is_email($_POST['email'])){
            $errors['email'] = __('Kérlek, valós email címet adj meg!', THEME_TEXTDOMAIN);
        }

        if(empty($_POST['terms'])){
            $errors['terms'] = 'Kérlek, fogadd el az adatkezelési tájékoztatóban foglaltakat!';
        }
        
        if(!empty($errors)){
            wp_send_json_error( array('errors' => $errors) );
        }
		
		//TODO: email cím kiszedése adminról
		$toAddress = 'hello@totya.eu';
		$toSubject = 'Kapcsolatfelvétel a weboldalról';
		$toMessage = twig_render('mail/contact.twig', $_POST, false);
		$toHeaders = array('Content-Type: text/html; charset=UTF-8');

        $result = wp_mail($toAddress, $toSubject, $toMessage, $toHeaders );

        if($result) {
            wp_send_json_success( array('script' => 'document.getElementById("contactform").reset();', 'notification' => 'Köszönjük megkeresését!') );
        }
        wp_send_json_success( array('script' => 'document.getElementById("contactform").reset();', 'notification' => 'Hoppá, valami hiba történt a küldés során. Kérlek, próbáld később újra, vagy keress minket közvetlenül valamelyik elérhetőségünkön!') );
    }
}

ContactForm::getInstance();