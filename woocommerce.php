<?php 

ob_start();
if ( is_singular( 'product' ) ) {
    woocommerce_content();
}else{
    woocommerce_get_template( 'archive-product.php' );
}
$woocommerceContent = ob_get_contents();
ob_end_clean();

twig_render('pages/woocommerce.twig', array('content' => $woocommerceContent));